# mms, a bootswatch theme

Copy the repo into the `/dist` folder of the [bootswatch](https://github.com/thomaspark/bootswatch.git) project for [customizing the theme](https://bootswatch.com/help/#customization).

